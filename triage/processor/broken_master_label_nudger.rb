# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/broken_master_incident_label_validator'
require_relative '../job/broken_master_label_nudger_job'

module Triage
  class BrokenMasterLabelNudger < Processor
    react_to 'issue.close'

    FIVE_MINUTES = 300

    def applicable?
      # TODO: also react to `issue.update` but only applicable if event.state == 'closed'
      # https://gitlab.com/gitlab-org/quality/triage-ops/-/issues/1194
      event.from_master_broken_incidents_project? &&
        !event.gitlab_bot_event_actor? &&
        (validator.need_root_cause_label? || validator.need_flaky_reason_label?)
    end

    def process
      BrokenMasterLabelNudgerJob.perform_in(FIVE_MINUTES, event)
    end

    def documentation
      <<~TEXT
        This processor reminds team member to provide concrete master-broken root cause label or flaky test reason label.
      TEXT
    end

    private

    def validator
      @validator ||= BrokenMasterIncidentLabelValidator.new(event)
    end
  end
end
